#!/bin/bash
# This script export the XFCE4 configuration

GIT_REPO=$(pwd)

printf "\n########## Remove old config folder ########\n"
rm -rf $GIT_REPO/config 2> /dev/null
mkdir $GIT_REPO/config

printf "\n########## Copy new config files ##########\n"
cp ~/.config/{gesturesmanger.json,libinput-gestures.conf} $GIT_REPO/config/

printf "\n########## Git push ##########\n"
git add config/*
git rm -r config/*
git commit -m "Update gesture-manager-x config"
git push origin master
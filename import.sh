#!/bin/bash
# This script import the Gesture-Manager-X configuration

printf "\n########## Install programs ##########\n"
paru -S --noconfirm --quiet gesture-manager-x-git libinput-gestures xdotool

printf "\n########### Add $USER to input group ###########"
sudo gpasswd -a $USER input

printf "\n########## Save old configuration folders ########\n"
rm -rf /tmp/gesture_config/ 2>/dev/null
mkdir /tmp/gesture_config
cp ~/.config/{gesturesmanger.json,libinput-gestures.conf} /tmp/gesture_config

printf "\n########## Copy new config files ##########\n"
wget https://gitlab.com/myscripts12/mygesture/-/raw/master/config/gesturesmanger.json -O ~/.config/gesturesmanger.json
wget https://gitlab.com/myscripts12/mygesture/-/raw/master/config/libinput-gestures.conf -O ~/.config/libinput-gestures.conf
sudo chown $USER:$GROUP ~/.config/{gesturesmanger.json,libinput-gestures.conf}
